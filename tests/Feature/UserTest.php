<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App;

/**
 * Class UserTest
 * @package Tests\Feature
 */
class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Create Passport Auth Token for User
     * @return array
     */
    private function createToken()
    {
        $user = App\User::find(1); //factory(App\User::class)->create();

        // Creating a token without scopes...
        $token = $user->createToken('Token Name')->accessToken;

        $header                  = [];
        $header['Accept']        = 'application/json';
        $header['Authorization'] = 'Bearer ' . $token;

        return $header;
    }

    /**
     * Test Create User for bad request.
     */
    public function testBadCreateUser()
    {
        $header  = $this->createToken(); // Create Auth Token and get headers for API Call.
        $request = $this->json('POST', '/api/user', ['first_name' => 'First Name'], $header); // Call API without passing mandatory inputs
        $request->assertStatus(200)->assertJson(['success' => false]); // Assert Status Code with "success" in JSON
    }

    /**
     * Test Create User for good request.
     * @throws mixed
     */
    public function testGoodCreateUser()
    {
        $header = $this->createToken(); // Create Auth Token and get headers for API Call.

        // Call API
        $request = $this->json('POST', '/api/user', [
            'first_name' => 'First Name', 'last_name' => 'Last Name', 'email' => 'first@last.com', 'password' => '123456'], $header);

        $request->assertStatus(200)->assertJson(['success' => true]); // Assert Status Code with "success" in JSON
        $response = $request->decodeResponseJson(); // Decode Response
        $this->assertContains('user', $response); // Assert added user get returned
        $this->assertEquals($response['user']['first_name'], 'First Name'); // Assert added user name is same input name.
    }

    /**
     * Test Get User for bad request.
     */
    public function testBadGetUser()
    {
        $header  = $this->createToken(); // Create Auth Token and get headers for API Call.
        $request = $this->json('GET', '/api/user/2', [], $header); // Call API inputting wrong User ID
        $request->assertStatus(200)->assertJson(['success' => false]); // Assert Status Code with "success" in JSON
    }

    /**
     * Test Get User for good request.
     * @throws mixed
     */
    public function testGoodGetUser()
    {
        $header = $this->createToken(); // Create Auth Token and get headers for API Call.

        // First: Call Create User API
        $request = $this->json('POST', '/api/user', [
            'first_name' => 'First Name', 'last_name' => 'Last Name', 'email' => 'first@last.com', 'password' => '123456'], $header);

        $response = $request->decodeResponseJson();
        $request  = $this->json('GET', '/api/user/' . $response['user']['id'], $header); // Second: Call Get User API inputting added User ID
        $request->assertStatus(200)->assertJson(['success' => true]); // Assert Status Code with "success" in JSON
        $response = $request->decodeResponseJson(); // Decode Response
        $this->assertContains('user', $response); // Assert added user get returned
        $this->assertEquals($response['user']['first_name'], 'First Name'); // Assert added user name is same as get user name.
    }

    /**
     * Test Update User for bad request.
     */
    public function testBadUpdateUser()
    {
        $header  = $this->createToken(); // Create Auth Token and get headers for API Call.
        $request = $this->json('PUT', '/api/user/2', [
            'first_name' => 'First Name'], $header); // Call API inputting wrong User ID
        $request->assertStatus(200)->assertJson(['success' => false]); // Assert Status Code with "success" in JSON
    }

    /**
     * Test Update User for good request.
     * @throws mixed
     */
    public function testGoodUpdateUser()
    {
        // Create Auth Token and get headers for API Call.
        $header = $this->createToken();

        // First: Call Create User API
        $request  = $this->json('POST', '/api/user', [
            'first_name' => 'First Name', 'last_name' => 'Last Name', 'email' => 'first@last.com', 'password' => '123456'], $header);
        $response = $request->decodeResponseJson();

        // Second: Call Update User API inputting added User ID
        $request = $this->json('PUT', '/api/user/' . $response['user']['id'], [
            'first_name' => 'First Name', 'last_name' => 'Last Name', 'email' => 'first@last.com', 'password' => '123456'], $header);
        $request->assertStatus(200)->assertJson(['success' => true]); // Assert Status Code with "success" in JSON
        $request  = $this->json('GET', '/api/user/' . $response['user']['id'], $header); // Third: Call Get User API inputting added User ID
        $response = $request->decodeResponseJson(); // Decode Response
        $this->assertContains('user', $response); // Assert updated user get returned
        $this->assertEquals($response['user']['first_name'], 'First Name'); // Assert updated user's detail with input provided
        $this->assertEquals($response['user']['last_name'], 'Last Name');
        $this->assertEquals($response['user']['email'], 'first@last.com');
    }

    /**
     * Test Delete User for bad request.
     */
    public function testBadDeleteUser()
    {
        $header  = $this->createToken(); // Create Auth Token and get headers for API Call.
        $request = $this->json('DELETE', '/api/user/2', [], $header); // Call Delete User API inputting wrong User ID
        $request->assertStatus(200)->assertJson(['success' => false]); // Assert Status Code with "success" in JSON
    }

    /**
     * Test Delete User for good request.
     * @throws mixed
     */
    public function testGoodDeleteUser()
    {
        $header   = $this->createToken(); // Create Auth Token and get headers for API Call.
        $request  = $this->json('POST', '/api/user', [
            'first_name' => 'First Name', 'last_name' => 'Last Name', 'email' => 'fist@last.com', 'password' => '123456'], $header); // First: Call Create User API
        $response = $request->decodeResponseJson();
        $request  = $this->json('DELETE', '/api/user/' . $response['user']['id'], $header); // Second: Call Delete User API inputting added User ID
        $request->assertStatus(200)->assertJson(['success' => true]); // Assert Status Code with "success" in JSON
        $request = $this->json('GET', '/api/user/' . $response['user']['id'], $header); // Third: Call Get User API inputting deleted User ID
        $request->assertStatus(200)->assertJson(['success' => false]); // Assert Status Code with "success" in JSON
    }

    /**
     * Test Get Users for route.
     */
    public function testGetUsersRoute()
    {
        $header   = $this->createToken(); // Create Auth Token and get headers for API Call.
        $response = $this->json('GET', '/api/user', [], $header);
        $response->assertStatus(200)->assertJson(['success' => true]);
    }

    /**
     * Test Get Users for good request.
     * @throws mixed
     */
    public function testGoodGetUsers()
    {
        $header = $this->createToken(); // Create Auth Token and get headers for API Call.

        // First: Call Create User API
        $this->json('POST', '/api/user', [
            'first_name' => 'First Name', 'last_name' => 'Last Name', 'email' => 'first@last.com', 'password' => '123456'], $header);
        $this->json('POST', '/api/user', [
            'first_name' => 'First Name', 'last_name' => 'Last Name', 'email' => 'first@last.com', 'password' => '123456'], $header);

        $request = $this->json('GET', '/api/user', [], $header); // Second: Call Get Users API
        $request->assertStatus(200)->assertJson(['success' => true]); // Assert Status Code with "success" in JSON
        $response = $request->decodeResponseJson(); // Decode Response
        $this->assertContains('user', $response); // Assert user get returned
        $this->assertEquals(count($response['user']), 2); // Assert number of users are same as number of added users
    }
}
