<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as Faker;
use App\Repositories\UserRepository;
use App;

/**
 * Class UserIntegrationTest
 * @package Tests\Feature
 */
class UserIntegrationTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test User Repository: Add User
     */
    public function testUserRepositoryAdd()
    {
        $faker          = Faker::create();
        $userRepository = new UserRepository();

        // Create User data using Factory
        factory(App\User::class, 2)->create()->each(
            function ($user) use ($userRepository, $faker) {
                $data             = $user->toArray();
                $data['email']    = $faker->unique()->safeEmail;
                $data['password'] = $faker->password();
                $response         = $userRepository->store($data); // Call "store" method of repo
                $this->assertTrue(!empty($response)); // Assert response of repo
            }
        );
    }

    /**
     * Test User Repository: Update User
     */
    public function testUserRepositoryUpdate()
    {
        $faker          = Faker::create();
        $userRepository = new UserRepository();
        $input          = [];

        // Create User data using Factory
        factory(App\User::class, 2)->create()->each(
            function ($user) use ($userRepository, $faker, $input) {
                // Modify data
                $input['first_name'] = $faker->name;
                $input['last_name']  = $faker->name;
                $input['email']      = $faker->email;
                $input['password']   = $faker->name;

                $response = $userRepository->update($user->id, $input); // Call "update" method of repo
                $this->assertTrue($response); // Assert response of repo
            });
    }

    /**
     * Test User Repository: Delete User
     */
    public function testUserRepositoryDelete()
    {
        $faker          = Faker::create();
        $userRepository = new UserRepository();

        // Create User data using Factory
        factory(App\User::class, 5)->create()->each(
            function ($user) use ($userRepository, $faker) {
                $response = $userRepository->destroy($user->id); // Call "destroy" method of repo
                $this->assertEquals(1, $response); // Assert response of repo
            });
    }

    /**
     * Test User Repository: Get User
     */
    public function testUserRepositoryGet()
    {
        $faker          = Faker::create();
        $userRepository = new UserRepository();

        // Create User data using Factory
        factory(App\User::class, 5)->create()->each(
            function ($user) use ($userRepository, $faker) {
                $response = $userRepository->getByID($user->id); // Call "getByID" method of repo
                $this->assertEquals(1, count($response)); // Assert response of repo
            });
    }

    /**
     * Test User Repository: Get Users
     */
    public function testUserRepositoryGetAll()
    {
        $userRepository = new UserRepository();
        factory(App\User::class, 5)->create(); // Create User data using Factory
        $response = $userRepository->getAll(); // Call "getAll" method of repo
        $this->assertEquals(6, count($response)); // Assert response of repo
    }
}
