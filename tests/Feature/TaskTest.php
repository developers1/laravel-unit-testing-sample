<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App;

/**
 * Class TaskTest
 * @package Tests\Feature
 */
class TaskTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Create Passport Auth Token for User
     * @return array
     */
    private function createToken()
    {
        $user = App\User::find(1); //factory(App\User::class)->create();

        // Creating a token without scopes...
        $token = $user->createToken('Token Name')->accessToken;

        $header                  = [];
        $header['Accept']        = 'application/json';
        $header['Authorization'] = 'Bearer ' . $token;

        return $header;
    }

    /**
     * Test Create Task for bad request.
     */
    public function testBadCreateTask()
    {
        // Create Auth Token and get headers for API Call.
        $header = $this->createToken();

        // Call API without passing mandatory inputs
        $request = $this->json('POST', '/api/task', ['name' => 'Task1'], $header);
        // Assert Status Code with "success" in JSON
        $request->assertStatus(200)->assertJson([
            'success' => false,
        ]);
    }

    /**
     * Test Create Task for good request.
     * @throws mixed
     */
    public function testGoodCreateTask()
    {
        // Create Auth Token and get headers for API Call.
        $header = $this->createToken();

        // Call API
        $request = $this->json('POST', '/api/task', ['name' => 'Task1', 'description' => 'Task1 Desc', 'completed' => 1], $header);
        // Assert Status Code with "success" in JSON
        $request->assertStatus(200)->assertJson([
            'success' => true,
        ]);
        // Decode Response
        $response = $request->decodeResponseJson();
        // Assert added task get returned
        $this->assertContains('task', $response);
        // Assert added task name is same input name.
        $this->assertEquals($response['task']['name'], 'Task1');
    }

    /**
     * Test Get Task for bad request.
     */
    public function testBadGetTask()
    {
        // Create Auth Token and get headers for API Call.
        $header = $this->createToken();

        // Call API inputting wrong Task ID
        $request = $this->json('GET', '/api/task/1', [], $header);
        // Assert Status Code with "success" in JSON
        $request->assertStatus(200)->assertJson([
            'success' => false,
        ]);
    }

    /**
     * Test Get Task for good request.
     * @throws mixed
     */
    public function testGoodGetTask()
    {
        // Create Auth Token and get headers for API Call.
        $header = $this->createToken();

        // First: Call Create Task API
        $request  = $this->json('POST', '/api/task', ['name' => 'Task1', 'description' => 'Task1 Desc', 'completed' => 1], $header);
        $response = $request->decodeResponseJson();

        // Second: Call Get Task API inputting added Task ID
        $request = $this->json('GET', '/api/task/' . $response['task']['id'], $header);
        // Assert Status Code with "success" in JSON
        $request->assertStatus(200)->assertJson([
            'success' => true,
        ]);
        // Decode Response
        $response = $request->decodeResponseJson();
        // Assert added task get returned
        $this->assertContains('task', $response);
        // Assert added task name is same as get task name.
        $this->assertEquals($response['task']['name'], 'Task1');
    }

    /**
     * Test Update Task for bad request.
     */
    public function testBadUpdateTask()
    {
        // Create Auth Token and get headers for API Call.
        $header = $this->createToken();

        // Call API inputting wrong Task ID
        $request = $this->json('PUT', '/api/task/1', ['name' => 'Task1', 'description' => 'Task1 Desc', 'completed' => 1], $header);
        // Assert Status Code with "success" in JSON
        $request->assertStatus(200)->assertJson([
            'success' => false,
        ]);
    }

    /**
     * Test Update Task for good request.
     * @throws mixed
     */
    public function testGoodUpdateTask()
    {
        // Create Auth Token and get headers for API Call.
        $header = $this->createToken();

        // First: Call Create Task API
        $request  = $this->json('POST', '/api/task', ['name' => 'Task1', 'description' => 'Task1 Desc', 'completed' => 1], $header);
        $response = $request->decodeResponseJson();

        // Second: Call Update Task API inputting added Task ID
        $request = $this->json('PUT', '/api/task/' . $response['task']['id'], ['name' => 'Task2', 'description' => 'Task2 Desc', 'completed' => 0], $header);
        // Assert Status Code with "success" in JSON
        $request->assertStatus(200)->assertJson([
            'success' => true,
        ]);

        // Third: Call Get Task API inputting added Task ID
        $request = $this->json('GET', '/api/task/' . $response['task']['id'], $header);
        // Decode Response
        $response = $request->decodeResponseJson();
        // Assert updated task get returned
        $this->assertContains('task', $response);
        // Assert updated task's detail with input provided
        $this->assertEquals($response['task']['name'], 'Task2');
        $this->assertEquals($response['task']['description'], 'Task2 Desc');
        $this->assertEquals($response['task']['completed'], '0');
    }

    /**
     * Test Delete Task for bad request.
     */
    public function testBadDeleteTask()
    {
        // Create Auth Token and get headers for API Call.
        $header = $this->createToken();

        // Call Delete Task API inputting wrong Task ID
        $request = $this->json('DELETE', '/api/task/1', [], $header);
        // Assert Status Code with "success" in JSON
        $request->assertStatus(200)->assertJson([
            'success' => false,
        ]);
    }

    /**
     * Test Delete Task for good request.
     * @throws mixed
     */
    public function testGoodDeleteTask()
    {
        // Create Auth Token and get headers for API Call.
        $header = $this->createToken();

        // First: Call Create Task API
        $request  = $this->json('POST', '/api/task', ['name' => 'Task1', 'description' => 'Task1 Desc', 'completed' => 1], $header);
        $response = $request->decodeResponseJson();

        // Second: Call Delete Task API inputting added Task ID
        $request = $this->json('DELETE', '/api/task/' . $response['task']['id'], $header);
        // Assert Status Code with "success" in JSON
        $request->assertStatus(200)->assertJson([
            'success' => true,
        ]);

        // Third: Call Get Task API inputting deleted Task ID
        $request = $this->json('GET', '/api/task/' . $response['task']['id'], $header);
        // Assert Status Code with "success" in JSON
        $request->assertStatus(200)->assertJson([
            'success' => false,
        ]);
    }

    /**
     * Test Get Tasks for route.
     */
    public function testGetTasksRoute()
    {
        // Create Auth Token and get headers for API Call.
        $header = $this->createToken();

        $response = $this->json('GET', '/api/task', [], $header);
        $response->assertStatus(200)->assertJson([
            'success' => true,
        ]);;
    }

    /**
     * Test Get Tasks for good request.
     * @throws mixed
     */
    public function testGoodGetTasks()
    {
        // Create Auth Token and get headers for API Call.
        $header = $this->createToken();

        // First: Call Create Task API
        $this->json('POST', '/api/task', ['name' => 'Task1', 'description' => 'Task1 Desc', 'completed' => 1], $header);
        $this->json('POST', '/api/task', ['name' => 'Task2', 'description' => 'Task2 Desc', 'completed' => 0], $header);

        // Second: Call Get Tasks API
        $request = $this->json('GET', '/api/task', [], $header);
        // Assert Status Code with "success" in JSON
        $request->assertStatus(200)->assertJson([
            'success' => true,
        ]);
        // Decode Response
        $response = $request->decodeResponseJson();
        // Assert task get returned
        $this->assertContains('tasks', $response);
        // Assert number of tasks are same as number of added tasks
        $this->assertEquals(count($response['tasks']), 2);
    }
}
