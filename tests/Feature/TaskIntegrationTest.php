<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as Faker;
use App\Repositories\TaskRepository;
use App;

/**
 * Class TaskIntegrationTest
 * @package Tests\Feature
 */
class TaskIntegrationTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test Task Repository: Add Task
     */
    public function testTaskRepositoryAdd()
    {
        $faker          = Faker::create();
        $taskRepository = new TaskRepository();

        // Create Task data using Factory
        factory(App\Task::class, 2)->create()->each(
            function ($task) use ($taskRepository, $faker) {
                // Call "store" method of repo
                $response = $taskRepository->store($task->toArray());
                // Assert response of repo
                $this->assertTrue(!empty($response));
            });
    }

    /**
     * Test Task Repository: Update Task
     */
    public function testTaskRepositoryUpdate()
    {
        $faker          = Faker::create();
        $taskRepository = new TaskRepository();
        $input          = [];

        // Create Task data using Factory
        factory(App\Task::class, 2)->create()->each(
            function ($task) use ($taskRepository, $faker, $input) {
                // Modify data
                $input['name']        = $faker->name;
                $input['description'] = $faker->text;
                $input['completed']   = $faker->randomElement($array = array('0', '1'));

                // Call "update" method of repo
                $response = $taskRepository->update($task->id, $input);
                // Assert response of repo
                $this->assertTrue($response);
            });
    }

    /**
     * Test Task Repository: Delete Task
     */
    public function testTaskRepositoryDelete()
    {
        $faker          = Faker::create();
        $taskRepository = new TaskRepository();

        // Create Task data using Factory
        factory(App\Task::class, 5)->create()->each(
            function ($task) use ($taskRepository, $faker) {
                // Call "destroy" method of repo
                $response = $taskRepository->destroy($task->id);
                // Assert response of repo
                $this->assertEquals(1, $response);
            });
    }

    /**
     * Test Task Repository: Get Task
     */
    public function testTaskRepositoryGet()
    {
        $faker          = Faker::create();
        $taskRepository = new TaskRepository();

        // Create Task data using Factory
        factory(App\Task::class, 5)->create()->each(
            function ($task) use ($taskRepository, $faker) {
                // Call "getByID" method of repo
                $response = $taskRepository->getByID($task->id);
                // Assert response of repo
                $this->assertEquals(1, count($response));
            });
    }

    /**
     * Test Task Repository: Get Tasks
     */
    public function testTaskRepositoryGetAll()
    {
        $taskRepository = new TaskRepository();

        // Create Task data using Factory
        factory(App\Task::class, 5)->create();

        // Call "getAll" method of repo
        $response = $taskRepository->getAll();
        // Assert response of repo
        $this->assertEquals(5, count($response));
    }
}
