# Laravel Unit Testing Sample
REST API for Users and Task
### Deployment notes for setting application in local environment:
1. git clone https://gitlab.com/developers1/laravel-unit-testing-sample
2. copy ".env.example" to ".env"
3. Change ".env" file for DB configuration and Mail configuration.
4. composer update
5. php artisan key:generate
6. php artisan migrate
7. php artisan db:seed
6. npm install
7. npm run dev
8. php artisan passport:client
9. php artisan passport:install
10. apt get phpunit

### PHP unit test command: 
1. phpunit **OR**
2. php vendor/phpunit/phpunit/phpunit

### API Endpoints:
#### Note:
Before requesting any API call, please add below request headers:

```sh
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjZjYzk0OWQwMDE2MzdmNTg4NGEzODdkZDFlYjAyYTdiNzViMGEwZTA2YmQ1NzZiMTExZjBmNGQzY2RmMWM3ODYyYzQ3MWU0N2FlNjFjY2M3In0.eyJhdWQiOiIyIiwianRpIjoiNmNjOTQ5ZDAwMTYzN2Y1ODg0YTM4N2RkMWViMDJhN2I3NWIwYTBlMDZiZDU3NmIxMTFmMGY0ZDNjZGYxYzc4NjJjNDcxZTQ3YWU2MWNjYzciLCJpYXQiOjE0OTgyMDE4MzgsIm5iZiI6MTQ5ODIwMTgzOCwiZXhwIjoxNTI5NzM3ODM4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.bDSGNFDndrzC7dlS_cHZOkVpPwW3X9INi9LPVC8wthfiOc8aO5AOv8o137vQ-v7dJ727iCPwYqDzm4PWtfX7tKZ025llMUao3ZhX7D57qrRz1qPP57prqBxtgQAKYQZq_RDmJ-9OCDZdIgRZ9Ar_W6hNnBCSUN9crXKHRgLauGT6Rb8MXpfUKKD48BipR5TtHqHTAR28OkJ_9XOSkadYb5Z8J3NYndnkvazR74a84y7Dgkfy2IUDFdjXpA66lWyTZx5XSiC4N2Xj1s48eZvAN4bPvAuwalCvFvqzR7O_J4J42PPC1u5U-ERTjuj8xBmzugNjvqVPCw_D8IOzV_v679vQ8DTqm0yHIhcWFrjiIUKgedvSKnCCcpy1AR9LNxQ2NANQkM8lszxRyaMdarkhlusUQYebQnr6tW0BorCKvBQLzBFA7vOITVmhY72GSSYhE1yiYLRIAkAO6_kfM4ZGbmX1BtBHu7qb1KNXqenflLUXFC5wcwI2_reYAnKIrMGfFHl71ghckTwfx8s8Jnvido6a0MA3IPRSbZfqNvVCC9p85bLFMLhry2dVecVxa5BrZoP7ZOer8eg_lO3fxDuUWPHbvOx7cN2AKsS5dogy0CLsnrwvviuYqAMeNenjRooHPkOzURs0U7mOa6KODvB_h_TvMTRBMQvydLfxOMyrfSg
Accept: application/json
```

#### 1. Users
##### 1. Get all Users
  * **Route:** http://66.175.214.21/laravel-unit-testing-sample/public/api/user
  * **Method:** GET
  * **Params:** NULL
  * **Result:** Collection

    ```sh
    {
       "success": true,
       "message": null,
       "users": [
                    {
                        "id": 1,
                        "first_name": "Dev",
                        "last_name": "Ast",
                        "email": "developers@arsenaltech.com",
                        "created_at": "2017-06-23 10:00:01",
                        "updated_at": "2017-06-23 10:00:01"
                    },
                    {
                        "id": 2,
                        "first_name": "Dev2",
                        "last_name": "Ast2",
                        "email": "developers2@arsenaltech.com",
                        "created_at": "2017-06-23 10:00:01",
                        "updated_at": "2017-06-23 10:00:01"
                    }
                ]
    }
    ```

##### 2. Get User
  * **Route:** http://66.175.214.21/laravel-unit-testing-sample/public/api/user/{id}
  * **Method:** GET
  * **Params:** NULL
  * **Result:** Collection

    ```sh
    {
       "success": true,
       "message": null,
       "user": {
                  "id": 1,
                  "first_name": "Dev",
                  "last_name": "Ast",
                  "email": "developers@arsenaltech.com",
                  "created_at": "2017-06-23 10:00:01",
                  "updated_at": "2017-06-23 10:00:01"
               }
    }
    ```

##### 3. Create User
  * **Route:** http://66.175.214.21/laravel-unit-testing-sample/public/api/user
  * **Method:** POST
  * **Params:**

    ```sh
    {
        "first_name": "Dev",
        "last_name": "Ast",
        "email": "developers@arsenaltech.com",
        "password": "123456"
    }
    ```

  * **Result:** Collection

    ```sh
    {
       "success": true,
        "message": "User Created Successfully.",
        "user": {
                   "first_name": "Dev",
                   "last_name": "Ast",
                   "email": "developers@arsenaltech.com",
                   "updated_at": "2017-06-23 11:16:57",
                   "created_at": "2017-06-23 11:16:57",
                   "id": 131
                }
    }
    ```

##### 4. Update User
  * **Route:** http://66.175.214.21/laravel-unit-testing-sample/public/api/user/{id}
  * **Method:** PUT
  * **Params:**

    ```sh
    {
        "first_name": "Dev",
        "last_name": "Ast",
        "email": "developers@arsenaltech.com"
    }
    ```

  * **Result:** collection

    ```sh
    {
       "success": true,
       "message": "User Info is updated."
    }
    ```

##### 5. Delete User
  * **Route:** http://66.175.214.21/laravel-unit-testing-sample/public/api/user/{id}
  * **Method:** DELETE
  * **Params:** NULL
  * **Result:** collection

    ```sh
    {
       "success": true,
       "message": "User Deleted Successfully."
    }
    ```

## 2. Tasks
##### 1. Get all Tasks
  * **Route:** http://66.175.214.21/laravel-unit-testing-sample/public/api/task
  * **Method:** GET
  * **Params:** NULL
  * **Result:** Collection

    ```sh
    {
       "success": true,
       "message": null,
       "tasks": [
                    {
                        "id": 1,
                        "name": "Task",
                        "description": "Task Desc",
                        "completed": "0",
                        "created_at": "2017-06-23 10:00:01",
                        "updated_at": "2017-06-23 10:00:01"
                    },
                    {
                        "id": 1,
                        "name": "Task 2",
                        "description": "Task Desc 2",
                        "completed": "1",
                        "created_at": "2017-06-23 10:00:01",
                        "updated_at": "2017-06-23 10:00:01"
                    }
                ]
    }
    ```

##### 2. Get Task
  * **Route:** http://66.175.214.21/laravel-unit-testing-sample/public/api/task/{id}
  * **Method:** GET
  * **Params:** NULL
  * **Result:** Collection

    ```sh
    {
       "success": true,
       "message": null,
       "task": {
                  "id": 1,
                  "name": "Task",
                  "description": "Task Desc",
                  "completed": "0",
                  "created_at": "2017-06-23 10:00:01",
                  "updated_at": "2017-06-23 10:00:01"
               }
    }
    ```

##### 3. Create Task
  * **Route:** http://66.175.214.21/laravel-unit-testing-sample/public/api/task
  * **Method:** POST
  * **Params:**

  ```sh
    {
        "name": "Task",
        "description": "Task Desc",
        "completed": "0"
    }
  ```

  * **Result:** Collection

    ```sh
    {
       "success": true,
       "message": "Task Created Successfully.",
       "task": 
              {
                 "name": "Task",
                 "description": "Task Desc",
                 "completed": "0"
                 "updated_at": "2017-06-23 11:16:57",
                 "created_at": "2017-06-23 11:16:57",
                 "id": 1
               }
    }
    ```

##### 4. Update Task
  * **Route:** http://66.175.214.21/laravel-unit-testing-sample/public/api/task/{id}
  * **Method:** PUT
  * **Params:**

    ```sh
    {
        "name": "Task",
        "description": "Task Desc",
        "completed": "0"
    }
    ```

  * **Result:** collection

    ```sh
    {
       "success": true,
       "message": "Task Info is updated."
    }
    ```

##### 5. Delete Task
  * **Route:** http://66.175.214.21/laravel-unit-testing-sample/public/api/task/{id}
  * **Method:** DELETE
  * **Params:** NULL
  * **Result:** collection

    ```sh
    {
       "success": true,
       "message": "Task Deleted Successfully."
    }
    ```
