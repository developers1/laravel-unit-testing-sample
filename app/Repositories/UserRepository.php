<?php
namespace App\Repositories;

use App\User;
use App\Interfaces\UserRepositoryInterface;

/**
 * Created by PhpStorm.
 * User: Arsenaltech
 * Date: 6/22/2017
 * Time: 4:15 PM
 */
class UserRepository implements UserRepositoryInterface
{
	public function store($request_data)
	{
		return User::create($request_data);
	}

	public function getByID($id)
	{
		return User::findOrFail($id);
	}

	public function update($id, $request_data)
	{
		$user = $this->getByID($id);
		return $user->update($request_data);
	}

	public function destroy($id)
	{
		return User::destroy($id);
	}

	public function getAll()
	{
		return User::all();
	}

}