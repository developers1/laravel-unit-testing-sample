<?php
namespace App\Repositories;
use App\Interfaces\TaskRepositoryInterface;
use App\Task;

/**
 * Created by PhpStorm.
 * User: Arsenaltech
 * Date: 6/22/2017
 * Time: 4:14 PM
 */
class TaskRepository implements TaskRepositoryInterface
{
	public function store($request_data)
	{
		return Task::create($request_data);
	}

	public function getByID($id)
	{
		return Task::findOrFail($id);
	}

	public function update($id, $request_data)
	{
		$task = $this->getByID($id);
		return $task->update($request_data);
	}

	public function destroy($id)
	{
		return Task::destroy($id);
	}

	public function getAll()
	{
		return Task::all();
	}


}