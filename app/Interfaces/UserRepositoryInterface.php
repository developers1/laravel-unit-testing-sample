<?php
namespace App\Interfaces;
/**
 * Created by PhpStorm.
 * User: Arsenaltech
 * Date: 6/22/2017
 * Time: 4:14 PM
 */
interface UserRepositoryInterface
{
	public function store($request_data);

	public function getByID($id);

	public function update($id, $request_data);

	public function destroy($id);

	public function getAll();

}