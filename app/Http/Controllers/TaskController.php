<?php

namespace App\Http\Controllers;

use App\Interfaces\TaskRepositoryInterface;
use Illuminate\Http\Request;

class TaskController extends Controller
{
	protected $task_repository;

	public function __construct(TaskRepositoryInterface $taskRepository)
	{
		$this->task_repository = $taskRepository;
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		try {
			$tasks = $this->task_repository->getAll();
			return response()->json(["success" => true, "message" => null, "tasks" => $tasks]);
		} catch (\Exception $e) {
			return response()->json(["success" => false, "message" => $e->getMessage(), "tasks" => null]);
		}
	}


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		try {
			$task = $this->task_repository->store($request->all());
			return response()->json(["success" => true, "message" => "Task Created Successfully.", "task" => $task]);
		} catch (\Exception $e) {
			return response()->json(["success" => false, "message" => $e->getMessage(), "task" => null]);
		}
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		try {
			$task = $this->task_repository->getByID($id);
			return response()->json(["success" => true, "message" => null, "task" => $task]);
		} catch (\Exception $e) {
			return response()->json(["success" => false, "message" => $e->getMessage(), "task" => null]);
		}

	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$request_data = $request->all();
		try {
			$task = $this->task_repository->update($id, $request_data);
			return response()->json(["success" => true, "message" => "Task Info is updated."]);
		} catch (\Exception $e) {
			return response()->json(["success" => false, "message" => $e->getMessage()]);
		}

	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		try {
			$task = $this->task_repository->getByID($id);
			$this->task_repository->destroy($id);
			return response()->json(["success" => true, "message" => "Task Deleted Successfully."]);
		} catch (\Exception $e) {
			return response()->json(["success" => false, "message" => $e->getMessage()]);
		}
	}
}
