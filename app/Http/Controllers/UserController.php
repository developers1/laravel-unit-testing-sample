<?php

namespace App\Http\Controllers;

use App\Notifications\UserRegistered;
use Illuminate\Http\Request;
use \App\Interfaces\UserRepositoryInterface;
use League\Flysystem\Exception;

class UserController extends Controller
{
	protected $user_repository;

	public function __construct(UserRepositoryInterface $userRepository)
	{
		$this->user_repository = $userRepository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		try {
			$user = $this->user_repository->getAll();
			return response()->json(["success" => true, "message" => null, "user" => $user]);
		} catch (\Exception $e) {
			return response()->json(["success" => false, "message" => $e->getMessage(), "users" => null]);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		try {
			$user = $this->user_repository->store($request->all());
			$user->notify(new UserRegistered($user));
			return response()->json(["success" => true, "message" => "User Created Successfully.", "user" => $user]);
		} catch (\Exception $e) {
			return response()->json(["success" => false, "message" => $e->getMessage(), "user" => null]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		try {
			$user = $this->user_repository->getByID($id);
			return response()->json(["success" => true, "message" => null, "user" => $user]);
		} catch (\Exception $e) {
			return response()->json(["success" => false, "message" => $e->getMessage(), "user" => null]);
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$request_data = $request->all();
		try {
			$user = $this->user_repository->update($id, $request_data);
			return response()->json(["success" => true, "message" => "User Info is updated."]);
		} catch (\Exception $e) {
			return response()->json(["success" => false, "message" => $e->getMessage()]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		try {
            $user = $this->user_repository->getByID($id);
			$this->user_repository->destroy($id);
			return response()->json(["success" => true, "message" => "User Deleted Successfully."]);
		} catch (\Exception $e) {
			return response()->json(["success" => false, "message" => $e->getMessage()]);
		}
	}
}
